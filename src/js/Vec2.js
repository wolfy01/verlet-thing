class Vec2 {
    constructor(x, y) {
        this.position_current = {
            x: x,
            y: y
        }
    }

    length() {
        return Math.sqrt(this.position_current.x * this.position_current.x + this.position_current.y * this.position_current.y);
    }

    distance(v) {
        let dx = v.x - this.position_current.x;
        let dy = v.y - this.position_current.y;
        return Math.sqrt(dx * dx + dy * dy);
    }
}