class Solver {
    constructor(renderer) {
        this.gravity = {
            x: 0.0,
            y: 1000.0
        };

        this.objects = renderer.objects;
    }

    update(dt) {
        this.applyGravity();
        this.applyConstraint();
        this.updatePositions(dt);
    }

    updatePositions(dt) {
        for (let index = 0; index < this.objects.circles.length; index++) {
            const object = this.objects.circles[index];
            object.updatePosition(dt);
        }
    }

    applyGravity() {
        for (let index = 0; index < this.objects.circles.length; index++) {
            const object = this.objects.circles[index];
            object.accelerate(this.gravity);
        }
    }

    applyConstraint() {
        const position = new Vec2(500/2, 50);
        const radius = 300;
        
        for (let index = 0; index < this.objects.circles.length; index++) {
            const obj = this.objects.circles[index];
            const to_obj = {
                x: obj.position_current.x - position.position_current.x,
                y: obj.position_current.y - position.position_current.y
            }

            const dist = obj.length();

            if (dist > radius - 50.0) {
                const n = {
                    x: to_obj.x / dist,
                    y: to_obj.y / dist
                }

                obj.position_current = {
                    x: position.position_current.x + n.x * (radius - 50.0),
                    y: position.position_current.y + n.y * (radius - 50.0)
                }
            }
        }
    }
}