class Circle extends Vec2 {
    constructor(x, y, radius) {
        super();
        this.radius = radius;

        this.position_current = { x: x, y: y };
        this.position_old = { x: x, y: y };
        this.acceleration = { x: 0, y: 0 };
    }

    updatePosition(dt) {
        const velocity = {
            x: this.position_current.x - this.position_old.x,
            y: this.position_current.y - this.position_old.y
        };

        this.position_old = this.position_current;

        this.position_current = {
            x: this.position_current.x + velocity.x + this.acceleration.x * dt * dt,
            y: this.position_current.y + velocity.y + this.acceleration.y * dt * dt
        }

        this.acceleration = {
            x: 0,
            y: 0
        };
    }

    accelerate(acc) {
        this.acceleration = {
            x: this.acceleration.x + acc.x,
            y: this.acceleration.y + acc.y
        }
    }
}