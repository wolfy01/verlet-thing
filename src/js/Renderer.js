class Renderer {
    constructor(canvas) {
        this.app = canvas;
        
        this.ctx = this.app.getContext("2d");

        this.renderLoop = null;
        
        this.objects = {
            circles: []
        };
    }

    resize(width, height) {
        this.app.width = width;
        this.app.height = height;
    }

    createRenderloop(fps) {
        if (this.renderLoop) clearInterval(this.renderLoop);

        this.renderLoop = setInterval(() => {
            this.render();
        }, 1000/fps);
    }

    drawCircle(circle) {
        const x = circle.position_current.x;
        const y = circle.position_current.y;
        const radius = circle.radius;
        
        this.ctx.beginPath();
        this.ctx.arc(x, y, radius, 0, 2 * Math.PI, false);
        this.ctx.fillStyle = "rgb(255,255,255)";
        this.ctx.fill();
    }

    render() {
        // this.ctx.clearRect(0, 0, this.app.width, this.app.height);
        this.ctx.fillStyle = "rgb(0,0,0)";
        this.ctx.fillRect(0, 0, this.app.width, this.app.height);
        
        for (let index = 0; index < this.objects.circles.length; index++) {
            const object = this.objects.circles[index];
            this.drawCircle(object);
        }
    }
}