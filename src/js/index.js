const renderer = new Renderer(document.createElement("canvas"));
renderer.resize(500, 500);
renderer.createRenderloop(60);

const solver = new Solver(renderer);

setInterval(() => {
    solver.update(0.016);
}, 1000/60);

function addCircle(x, y, radius) {
    const circle = new Circle(x, y, radius);
    renderer.objects.circles.push(circle);
}

addCircle(100, 50, 10);

document.body.appendChild(renderer.app);